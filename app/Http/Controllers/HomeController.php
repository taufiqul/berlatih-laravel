<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function register()
    {
        return view('data.register');
    }

    public function welcome(Request $request)
    {
        $name = $request->placeholder1;
        $name2 = $request->placeholder2;

        return view('data.welcome' , compact('name' , 'name2'));
    }

}
