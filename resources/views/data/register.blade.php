<!DOCTYPE html>
<html>
<head>
  <title> Form </title>
  <meta charset="UTF-8">
</head>
<body>

  <h1>Buat Account Baru!</h1>
  <h2>Sign Up Form</h2>

  <!--Membuat form-->
  <form action="/register" method="POST">
    @csrf
    <label for="first_name">First name:</label> <br><br>
    <input type="text" name="placeholder1" id="first_name">
    <br><br>
    <label for="last_name">Last name:</label> <br> <br>
    <input type="text" name="placeholder2" id="last_name">
    <br><br>
    <label>Gender:</label> <br><br>
    <input type="radio" name="gender" value="0"> Male <br>
    <input type="radio" name="gender" value="1"> Female <br>
    <input type="radio" name="gender" value="2"> Other <br><br>
    <label>Nationality:</label> <br> <br>
    <select>
      <option value="indonesian">Indonesian</option>
      <option value="singapore">Singapore</option>
      <option value="malaysian">Malaysian</option>
      <option value="australia">Australia</option>
    </select>
    <br><br>
    <label>Language Spoken:</label> <br> <br>
    <input type="checkbox" name="language" value="0"> Bahasa Indonesia <br>
    <input type="checkbox" name="language" value="1"> English <br>
    <input type="checkbox" name="language" value="2"> Other <br><br>
    <label for="bio">Bio:</label> <br> <br>
    <textarea name="bio" rows="8" cols="30"></textarea>
    <br>
      <input type="submit" value="Sign Up">
  </form>
